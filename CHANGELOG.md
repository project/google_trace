# Google Trace

## 1.0.2 - 28/03/2021
- fix missing readme file...

## 1.0.1 - 28/03/2021
- Add readme file

## 1.0.0 - 28/03/2021
- Create initial version
- Create changelog
- Remove setting form
