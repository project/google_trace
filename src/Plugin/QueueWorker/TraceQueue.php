<?php

namespace Drupal\google_trace\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\google_trace\TraceHelper;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends queued traces to Google Cloud.
 *
 * @QueueWorker(
 *   id = "google_trace_queue",
 *   title = @Translation("Google Trace queue"),
 *   cron = { "time" = 60 }
 * )
 */
class TraceQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  protected TraceHelper $traceHelper;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TraceHelper $traceHelper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->traceHelper = $traceHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('google_trace.helper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    try {
      /** @var \Google\Cloud\Trace\Trace $trace */
      $trace = $item['trace'];
      $this->traceHelper->insertTrace($trace);
    }
    catch (Exception $e) {
      \Drupal::logger('google_trace')->error('Error @code inserting trace : @message', ['@code' => $e->getCode(), '@message' => $e->getMessage()]);
    }
  }

}
