<?php

namespace Drupal\google_trace;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Google\Cloud\Trace\Trace;
use Google\Cloud\Trace\TraceClient;

/**
 * Trace service.
 */
class TraceHelper {

  protected ?TraceClient $traceClient;

  protected QueueInterface $queue;

  protected LoggerChannelInterface $loggerChannel;


  public function __construct(QueueFactory $queueFactory, LoggerChannelInterface $loggerChannel) {
    $this->queue = $queueFactory->get('google_trace_queue');
    $this->loggerChannel = $loggerChannel;
    $this->traceClient = NULL;
    try {
      $this->traceClient = new TraceClient();
    }
    catch (\Exception $e) {
      $this->loggerChannel->critical(t('TraceClient Error : @code <code>@message</code>', ['@code' => $e->getCode(), '@message' => $e->getMessage()]));
    }
  }

  public function getTrace() : ?Trace {
    if ($this->traceClient instanceof TraceClient) {
      return $this->traceClient->trace();
    }
    $this->loggerChannel->critical(t('TraceClient not initialized'));
    return NULL;
  }

  public function insertTrace(Trace $strace) {
    if ($this->traceClient instanceof TraceClient) {
      return $this->traceClient->insert($strace);
    }
    $this->loggerChannel->critical(t('TraceClient not initialized'));
    return FALSE;
  }

  public function queueTrace(Trace $trace) {
    $this->queue->createItem(['trace' => $trace]);
  }

}
