# Google Cloud Logging

Integration of Google Trace tool with drupal.

This module require [`google/google_trace`](https://github.com/googleapis/google-cloud-php-trace) lib in order to works.

## Init

Two environment variables need to be set for the logger to work:

- `GOOGLE_APPLICATION_CREDENTIALS` : Relative path from Drupal root to JSON file containing authentication credentials (e.g. : `../files/my-project-5115.json`)
- `GOOGLE_CLOUD_PROJECT` : Google Cloud Project name (e.g. : `my-gcp-project-5115`)

See the [Google Cloud authentication doc](https://github.com/googleapis/google-cloud-php/blob/master/AUTHENTICATION.md) for details
